﻿using System.Linq;
using Common;
using Common.Models;
using RestDemo.DataTransferObjects;
using RestDemo.Parameters;

namespace RestDemo.Queries
{
    public class TaskStatusQuery : IQueryParametrized<TaskStatusParameter, TaskStatusDto>
    {
        public TaskStatusDto Execute(TaskStatusParameter parameter)
        {

            using (var context = new DemoContext())
            {
                var task = context.Set<TaskJournal>().FirstOrDefault(t => t.Id == parameter.Id);

                if (task == null)
                    return TaskStatusDto.Empty;

                return new TaskStatusDto
                {
                    Status = task.Status.ToString().ToLower(),
                    TimeStamp = task.TimeStamp.ToString("o")
                };
            }
        }
    }
}