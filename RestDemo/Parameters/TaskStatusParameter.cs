﻿using System;

namespace RestDemo.Parameters
{
    public class TaskStatusParameter
    {
        public Guid Id { get; set; }
    }
}