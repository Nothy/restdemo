﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Common;
using RestDemo.Commands;
using RestDemo.DataTransferObjects;
using RestDemo.Handlers;
using RestDemo.Parameters;
using RestDemo.Queries;

namespace RestDemo
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            IocConfigurate();

            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void IocConfigurate()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //builder.RegisterWebApiFilterProvider(config);
            //builder.RegisterWebApiModelBinderProvider();
            
            

            builder.RegisterType<CreateTaskHander>().As<IHandlerParametrizedResult<CreateTaskCommand, CreateTaskResultDto>>();
            builder.RegisterType<ChangeStatusHandler>().As<IHandlerParametrized<ChangeStatusCommand>>();
            builder.RegisterType<TaskStatusQuery>().As<IQueryParametrized<TaskStatusParameter, TaskStatusDto>>();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
