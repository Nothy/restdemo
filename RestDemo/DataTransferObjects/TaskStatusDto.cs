﻿namespace RestDemo.DataTransferObjects
{
    public class TaskStatusDto
    {
        public string Status { get; set; }
        public string TimeStamp { get; set; }

        public static TaskStatusDto Empty { get; } = new TaskStatusDto();
    }
}