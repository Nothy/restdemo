﻿using System;

namespace RestDemo.DataTransferObjects
{
    public class CreateTaskResultDto
    {
        public Guid Id { get; set; }
    }
}