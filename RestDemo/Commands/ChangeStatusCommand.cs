﻿using System;
using Common.Models;

namespace RestDemo.Commands
{
  public class ChangeStatusCommand
  {
    public Guid Id { get; set; }
    public Statuses Status { get; set; }
  }
}