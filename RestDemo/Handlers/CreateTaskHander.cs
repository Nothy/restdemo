﻿using System;
using Common;
using Common.Models;
using RestDemo.Commands;
using RestDemo.DataTransferObjects;

namespace RestDemo.Handlers
{
    public class CreateTaskHander : IHandlerParametrizedResult<CreateTaskCommand, CreateTaskResultDto>
    {
        public CreateTaskResultDto Execute(CreateTaskCommand parameter)
        {
            using (var context = new DemoContext())
            {
                var newTaskJournalEntry = new TaskJournal
                {
                    Status = Statuses.Created,
                    TimeStamp = DateTime.Now
                };

                context.Set<TaskJournal>().Add(newTaskJournalEntry);
                context.SaveChanges();

                return new CreateTaskResultDto { Id = newTaskJournalEntry.Id };
            }
        }
    }
}