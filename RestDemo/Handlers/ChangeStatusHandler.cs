﻿using System.Linq;
using Common;
using Common.Models;
using RestDemo.Commands;

namespace RestDemo.Handlers
{
    public class ChangeStatusHandler : IHandlerParametrized<ChangeStatusCommand>
    {
        public void Execute(ChangeStatusCommand command)
        {
            using (var context = new DemoContext())
            {
                var task = context.Set<TaskJournal>().FirstOrDefault(t => t.Id == command.Id);

                if (task == null)
                    return;

                task.Status = command.Status;
                context.SaveChanges();
            }
        }
    }
}