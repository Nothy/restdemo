﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using Common;
using Common.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestDemo.Commands;
using RestDemo.DataTransferObjects;
using RestDemo.Parameters;

namespace RestDemo.Controllers
{
  public class DemoRestController : ApiController
  {
    public DemoRestController(
        IHandlerParametrizedResult<CreateTaskCommand, CreateTaskResultDto> createTaskHander,
        IHandlerParametrized<ChangeStatusCommand> changeStatusHandler,
        IQueryParametrized<TaskStatusParameter, TaskStatusDto> taskStatusQuery)
    {
      this.createTaskHander = createTaskHander;
      this.changeStatusHandler = changeStatusHandler;
      this.taskStatusQuery = taskStatusQuery;
    }

    private readonly IHandlerParametrizedResult<CreateTaskCommand, CreateTaskResultDto> createTaskHander;
    private readonly IHandlerParametrized<ChangeStatusCommand> changeStatusHandler;

    private readonly IQueryParametrized<TaskStatusParameter, TaskStatusDto> taskStatusQuery;

    [Route("task")]
    public HttpResponseMessage Post()
    {
      var command = new CreateTaskCommand();
      var taskDto = createTaskHander.Execute(command);

      var task = new Task(() =>
          {
            var toRunnigCommand = new ChangeStatusCommand { Id = taskDto.Id, Status = Statuses.Running};
            changeStatusHandler.Execute(toRunnigCommand);

            Thread.Sleep(2 * 60 * 1000);

            var toFinishedCommand = new ChangeStatusCommand { Id = taskDto.Id, Status = Statuses.Finished};
            changeStatusHandler.Execute(toFinishedCommand);
          });

      var fault = task
          .ContinueWith(previousTask =>
          {
            // logging about previousTask.Exception.InnerExceptions
          }, TaskContinuationOptions.OnlyOnFaulted);

      task.Start();

      var response = Request.CreateResponse(HttpStatusCode.Accepted, taskDto.Id);
      return response;
    }

    [Route("task/{id}")]
    public HttpResponseMessage Get(string id)
    {
      if (!Guid.TryParse(id, out var taskId))
        return Request.CreateResponse(HttpStatusCode.BadRequest);

      var parameter = new TaskStatusParameter { Id = taskId };
      var result = taskStatusQuery.Execute(parameter);

      if (result == TaskStatusDto.Empty)
        return Request.CreateResponse(HttpStatusCode.NotFound);

      var serializerSettings = new JsonSerializerSettings();
      serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
      var jsonResult = JsonConvert.SerializeObject(result, serializerSettings);

      return Request.CreateResponse(HttpStatusCode.NotFound, jsonResult);
    }
  }
}
