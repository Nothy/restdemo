﻿using System.Data.Entity;
using Common.Models;

namespace Common
{
    public class DemoContext : DbContext
    {
        public DemoContext() : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskJournal>().HasKey(h => h.Id);
        }
    }
}
