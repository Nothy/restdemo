﻿namespace Common
{
    public interface IQueryParametrized<T, R>
    {
        R Execute(T parameter);
    }
}
