﻿namespace Common
{
    public interface IHandlerParametrizedResult<T, R>
    {
        R Execute(T parameter);
    }
}
