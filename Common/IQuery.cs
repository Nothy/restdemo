﻿namespace Common
{
    public interface IQuery<T>
    {
        T Execute();
    }
}
