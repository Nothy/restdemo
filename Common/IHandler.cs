﻿namespace Common
{
    public interface IHandler
    {
        void Execute();
    }
}
