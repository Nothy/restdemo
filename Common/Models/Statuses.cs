﻿namespace Common.Models
{
    public enum Statuses
    {
        Created = 1,
        Running = 2,
        Finished = 3
    }
}