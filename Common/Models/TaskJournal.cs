﻿using System;

namespace Common.Models
{
    public class TaskJournal
    {
        public TaskJournal()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public Statuses Status { get; set; }
    }
}