﻿namespace Common
{
    public interface IHandlerParametrized<T>
    {
        void Execute(T command);
    }
}
